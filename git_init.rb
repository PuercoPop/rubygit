require 'fileutils'

def do_git_init(path)
  if !path
    path =  Dir.pwd
  else
    path = File.join Dir.pwd, path
  end
  #Create required files
  if !Dir.exist? path
    Dir.mkdir path
  end

  p 'Initialized empty Git repository in ' + path
  dirs = [
      '.git\\objects\\info\\',
      '.git\\objects\\pack\\',
      '.git\\refs\\heads\\',
      '.git\\refs\\ tags\\',
  ]
  files = {
      '.git/HEAD'=>'ref: refs/heads/main',
      '.git/config'=> %{[core]
  repositoryformatversion = 0
  filemode = false
  bare = false
  logallrefupdates = true
  symlinks = false
  ignorecase = true},
  }
  #Create dirs
  dirs.each do |subdir|
    subpath = File.join path,subdir
    if !Dir.exist? subpath
      FileUtils.mkdir_p subpath
    end
  end

  #Create files
  files.each do |key, value|
    filedir = File.join path,key
    File.open(filedir, "w") { |f| f.write value }
  end
end

