# ARGV
require './git_init.rb'
require './git_hash_object.rb'
require './git_update_index.rb'
require './git_commit.rb'

if ARGV[0] == 'init'
  do_git_init(ARGV[1])
end

if ARGV[0] == 'hash-object'
  do_git_hash_object(ARGV[1])
end
if ARGV[0] == 'update-index'
  do_update_index(ARGV[1..-1])
end

if ARGV[0] == 'commit'
  do_commit(ARGV[1..-1])
end
