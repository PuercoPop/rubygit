require 'digest/sha1'
def do_commit (params)
  commit_message = params[0]
  #Creamos un tree object
  contents = ""
  params[1..-1].each  do |file_sub_path|
    contents += "100644 "
    contents += File.basename(file_sub_path)
    contents += "\0"
    file_path = File.join Dir.pwd, file_sub_path
    contents +=  generate_hash(file_path)
  end
  body = "tree #{contents.length}\0#{contents}"
  sha1 = Digest::SHA1.hexdigest body
  file_dir = File.join Dir.pwd , '.git', 'objects',sha1[0..1]
  file_name = sha1[2..-1]
  FileUtils.mkdir_p file_dir
  file_path = File.join file_dir, file_name
  value = Zlib::Deflate.deflate body
  File.open(file_path, "w") { |f| f.write value }

  #Create commit object
  time2 = Time.now
  offset =  time2.utc_offset/60/60
  xcommit= %{tree #{sha1}
author Your Name <you@example.com> #{time2.to_i} -0500
committer Your Name <you@example.com> #{time2.to_i} -0500

#{commit_message}
}
  newbody = "commit #{xcommit.length}\0#{xcommit}"

  sha3 = Digest::SHA1.hexdigest newbody
  file_dir4 = File.join Dir.pwd , '.git', 'objects',sha3[0..1]
  file_name2 = sha3[2..-1]
  FileUtils.mkdir_p file_dir4
  file_path3 = File.join file_dir4, file_name2
  value4 = Zlib::Deflate.deflate newbody
  File.open(file_path3, "wb") { |f| f.write value4 }
  #Point commit to branch
  File.open(File.join(Dir.pwd,".git\\refs\\heads\\rubyperu"), "w") { |f| f.write sha3 }
  File.open(File.join(Dir.pwd,".git\\HEAD"), "w") { |f| f.write "ref: refs/heads/rubyperu" }

end

def generate_hash(file)
  file = File.open(file, "rb")
  contents = file.read
  file.close
  body = "blob #{contents.length}\0#{contents}"

  sha1 = Digest::SHA1.digest body
end
