require 'fileutils'
require "zlib"
require 'digest/sha1'

def do_git_hash_object(file_sub_dir)
  file_path = File.join Dir.pwd, file_sub_dir
  if !File.exists? file_path
    p "fatal: Cannot open '#{file_sub_dir}': No such file or directory"
    return
  end
  #Read file
  file = File.open(file_sub_dir, "rb")
  contents = file.read
  file.close
  body = "blob #{contents.length}\0#{contents}"
  sha1 = Digest::SHA1.hexdigest body
  file_dir = File.join Dir.pwd , '.git', 'objects',sha1[0..1]
  file_name = sha1[2..-1]
  FileUtils.mkdir_p file_dir
  file_path = File.join file_dir, file_name
  value = Zlib::Deflate.deflate body
  File.open(file_path, "w") { |f| f.write value }
  return sha1
end
